const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    filename: "main.js",
    path: path.resolve(__dirname, "dist")
  },
  devServer: {
    contentBase: "./dist",
    open: "Firefox"
  },
  module: {
    rules: [{ test: /\.js$/, exclude: /node_modules/, use: ["babel-loader", "eslint-loader"] }]
  }
};
