import '@babel/polyfill';
import WorldcupService from './services/worldcup-service';
import MatchesTable from './components/matchestable/matchestable-component';

const worldcupService = new WorldcupService();

worldcupService
    .getMatches()
    .then(data => {

        const matchesTable = new MatchesTable(data);
        const dom = matchesTable.render();
        document.body.appendChild(dom);

    })
    .catch(err => {document.body.innerHTML = err;});
