export default class MatchesTable {
    constructor(data) {
        this.data = data;
    }

    render() {
        let dom = document.createElement('table');
        
        this.data.forEach(m => {
            const row = `<tr>
                            <td>${m.home_team.country}</td>
                            <td>${m.away_team.country}</td>
                            <td>${m.home_team.goals} - ${m.away_team.goals} </td>
                        </tr>`;
            dom.innerHTML += row;
        });
        return dom;
    }

} 