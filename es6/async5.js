let url = 'https://worldcup.sfg.io/matches';

// let xhr = new XMLHttpRequest();

// xhr.open('GET', url);

// xhr.onreadystatechange = function() {
//    if(xhr.readyState === 4) {
//        console.log(xhr.responseText);
//    } 
// }
// xhr.send(null);


function myfetch(url) {
    let p = new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url);

        xhr.onerror = e => reject(new Error(e));

        xhr.onreadystatechange = () => {
           if(xhr.readyState === 4) {
               resolve(xhr);
           } 
        }
        xhr.send(null);
    });

    return p;
}

myfetch('https://worldcup.sfg.io/matches')
    .then(response => JSON.parse(response.responseText))
    .then(value => console.log(value));

    // // then retourne une promesse
    // then(
    //     value => console.log(value) // return undefined
    //     // retourne une promesse résolue avec la valeur undefined
    // )
    // .then(
    //     value =>  42
    // )
    // .then(
    //     // value = 42
    //     value => new Promise((resolve, reject) => { resolve('hello')})
    // ).then(
    //    // value = 'hello 
    //    value => console.log(value)
    // )