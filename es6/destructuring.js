const o = {
    firstname: 'John',
    lastname: 'Doe',
    age: 24
}

const {firstname, lastname} = o;
console.log(firstname, lastname);

function render({firstname, lastname}) {
    console.log(firstname, lastname);
}

render(o);

let data = [1,2,3,4];
let [un, , trois] = data;
console.log(un, trois);

// rest
let [u, d, ...r] = data;
console.log(u, d, r);

function f(x, ...p) {
    console.log(x, p);
}

f(1,2,3,4,5);

// spread
console.log(Math.max(...data));

let data2 = [10, 20, 30];
data2.push(...data);
console.log(data2);
let data3 = [0, ...data, 10];
console.log(data3);



