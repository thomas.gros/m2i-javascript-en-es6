// pre ES 6
let s1 = "une chaîne de caractères";
let s2 = 'une chaîne de caractères';

// ES 6 => backticks ``   (AltGr + 7)
let s3 = `une chaîne de caractères`;
console.log(s3);


// caractéristique 1: intégration d'expression ECMAScript via ${}
let x = 10;
let s4 = `x + 1 = ${x + 1}`;
console.log(s4);

// caractéristique 2: multi-lignes
let o = {
    name: 'John Doe',
    description: 'la description de John...'
}
let s5 = `<div>
            <h2>${o.name}</h2>
            <p>${o.description}</p>
          </div>`;

console.log(s5);

// tagged template strings
// équivalent aux moteurs de template type mustache, etc...

function buildString(txt, ...values) {
    console.log(txt);
    console.log(...values);
    return `hello ${arguments[1]}, ${arguments[2]}`;
}

let n1 = 'didier';
let n2 = 'marcel';
let res = buildString`les prenoms ${n1} et ${n2} :-)`;
console.log(res);


function buildHTML(txt, ...values) {
    console.log(txt);
    console.log(values);
    let res = `<p>`;
    res += txt[0];
    res += values[0];
    res += txt[1];
    res += `</p>`;
    return res;
}

let data = 'lorem ipsum...';
let html = buildHTML`${data}`;
console.log(html);

html = buildHTML`<span>${data}</span>`;
console.log(html);


