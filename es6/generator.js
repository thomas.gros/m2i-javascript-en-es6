function* gen() {
    console.log("debut du generateur");
    let v = yield 'premier yield';
    console.log(v);
    let x = yield 'deuxième yield';
    console.log(x);
    let z = yield 'troisième yield';
    console.log(z);
    console.log("fin du generateur");
}

let g = gen();
let data = g.next(); // pas de data sur le premier next
console.log(data);
data = g.next('hello 1');
console.log(data);
data = g.next('hello 2');
console.log(data);
data = g.next('hello 3');
console.log(data);

for(let d of g) {
    console.log(d);
}

let f = new File('c://..')
for(let d of f) {
    console.log(d);
}