const url = 'https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=fcdbc791515ee6657a2c7fbae4d56ba0&tags=landscape&format=json&nojsoncallback=1';

// const promise = fetch(url);
// promise
//     .then(response => response.json())
//     .then(json => console.log(json))
//     .catch(err => console.log(err));

async function apiCall(url) {
    try {
        let response = await fetch(url);
        let json = await response.json();
        return json;
    } catch (err) {
        console.log(err);
        return err;
    } 
    
} 

console.log('debut');
apiCall(url).then(console.log);
console.log('fin');