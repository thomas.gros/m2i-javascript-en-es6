// en ES6 on déclare des variables avec let
// et on peut déclarer des 'constantes' avec const

const x = 42;
console.log(x);

// x = 100; // TypeError: invalid assignment to const `x'

// une constante ES6 déclare une référence read-only vers une valeur.
// Ca ne signifie pas que la valeur référencée est immutable, mais
// juste que l'identifiant ne peut pas être réassigné.
const y = [1,2,3,4,5];
y.push(12); // les constantes ne sont pas immutables
console.log(y);

const o = {name: 'Bob'}
o.age = 50; // les constantes ne sont pas immutables
console.log(o);  


// Pour rendre les propriétés immutables, utiliser:
// - writable: false
// - Object.seal Object.freeze (https://stackoverflow.com/questions/21402108/difference-between-freeze-and-seal-in-javascript)