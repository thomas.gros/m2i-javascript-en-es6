async function hello() {
    return 'hello'
}

async function world() {
    return 'world';
} 


// hello()
// .then(value => {
//     console.log(value);
//     return world();
// })
// .then(value => {
//     console.log(value);
// });

async function doSomeWork() { 
    let value = await hello();
    console.log(value);
    value = await world();
    console.log(value);
}

doSomeWork()
    .then(() => console.log('done'));