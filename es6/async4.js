console.log('debut');

function timeout(time) {
    let p = new Promise((resolve, reject) => {
        setTimeout(resolve, time);
    });
    return p;
}

timeout(500)
    .then(() => {
        console.log('done 1');
        return timeout(1000)
     })
     .then(() => {
        console.log('done 2');
        return timeout(1500)
     })
     .then(() => {
        console.log('done 3');
     })
     ;

// setTimeout(function() {
//     console.log('working...');
//     setTimeout(function() {
//         console.log('working...')
//         setTimeout(function() {
//             console.log('working...')
//         }, 3000);
//     }, 2000);
// }, 1000);

console.log('fin');