// function Person(firstname, lastname) {
//     this.firstname = firstname;
//     this.lastname = lastname;
// } 

// Person.prototype.fullname = function() {
//     return this.firstname + ' ' + this.lastname;
// }

class Person {
    constructor(firstname, lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }
    fullname() {
        return this.firstname + ' ' + this.lastname;
    }
}

const p1 = new Person('John', 'Doe');
const p2 = new Person('Sally', 'Smith');
console.log(p1.fullname());
console.log(p2.fullname());