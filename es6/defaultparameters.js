function foo(x=0, y=0) {
    console.log(x, y);
} 

foo();
foo(1);
foo(1,2);

function bar({start=0, end=-1, step=1}) {
    console.log(start, end, step);
}
const options = {start: 0, step:3};
bar(options);