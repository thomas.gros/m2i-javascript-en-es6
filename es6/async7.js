// https://worldcup.sfg.io/matches
// https://worldcup.sfg.io/matches/today
// https://worldcup.sfg.io/matches/current

let p1 = fetch('https://worldcup.sfg.io/matches');
let p2 = fetch('https://worldcup.sfg.io/matches/today');
let p3 = fetch('https://worldcup.sfg.io/matches/current');

Promise
    .all([p1,p2,p3])
    .then(value => console.log('value'))
    .catch(console.log)