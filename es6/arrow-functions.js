// fonctions traditionnelles
function sum(a,b) {
    return a + b;
} 

console.log(sum(1,2));

// ES6: arrow functions

// syntaxe plus concixe
let sum2 = (a,b) => {
    return a + b;
}
console.log(sum2);
console.log(sum2(1,2));

// autre exemple
const data = [1,2,3,4,5];
let result = 
  // data.map(function(e) { return e * 2} );
  // data.map((e) => { return e * 2});
  // data.map(e => { return e * 2});
     data.map(e => e * 2);

console.log(result);

result = data
    .map(e => e * 2)
    .filter(e => e > 3)
    .reduce((acc, e) => acc + e, 0);

const multBy2 = e => e * 2;
const isSup3 = e => e > 3
const sum = (acc, e) => acc + e;

result = data
    .map(multBy2)
    .filter(isSup3)
    .reduce(sum, 0);


console.log(result);

// lexical variables
// this, arguments, super, new.target

// cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Fonctions/Fonctions_fléchées
function Person() {
    this.age = 0;
    // setInterval(function grow() {
    //     console.log(this);
    //     this.age++;
    // }, 1000);

    setInterval(() => {
        console.log(this);
        this.age++;
    }, 1000)
} 


let p = new Person();

let o = {
    age: 42,
    // getAge: () => this.age, // NON
    getAge() {
        return this.age
    }   
}  