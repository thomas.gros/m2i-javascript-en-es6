const data = [1,2,3,4,5];

for(let i = 0; i < data.length; i++) {
    console.log(data[i]);
    if(data[i] == 3) {
        console.log('trouvé');
        break;
    }
}

data.forEach(e => {
    console.log(e);
});

data.forEach(console.log);

// ES6: concis + break
for(let e of data) {
    console.log(e);
}

for(let [index, e] of data.entries()) {
    console.log(index, e);
}